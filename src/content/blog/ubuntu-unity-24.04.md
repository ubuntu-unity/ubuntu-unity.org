---
pubDatetime: 2024-04-25T00:00:00
title: "Ubuntu Unity 24.04 released"
description: "Ubuntu Unity 24.04 has now been released."
author: "Rudra Saraswat"
draft: false
featured: true

tags:
  - flavor
  - release
  - stable
  - lts
---

Ubuntu Unity 24.04 "Noble Numbat" has now been released! You can download it from https://ubuntuunity.org.

![Ubuntu Unity 24.04 screenshot](../../assets/images/ubuntu-unity-24.04.png)

Ubuntu Unity 24.04 continues to use Unity 7.7, which has undergone maintenance. Our primary focus for this release has been to have a working Lomiri variant in collaboration with the UBports Foundation, to serve as an alternative to Unity7 since we're stuck with X11 for the time-being. This requirement arises from certain dependencies, including Nux, the UI toolkit used for rendering Unity7's user interfaces. UnityX too uses Nux unfortunately (since it's a fork of the Unity7 codebase), and so while it can be made to run under stacking Wayland compositors like Wayfire and labwc, it'd continue to rely on X (Xwayland in this case), and so would only act as a stop-gap. Getting around this would mean an entire rewrite, and so while Unity7 isn't going away anytime soon, Lomiri would act as a suitable replacement if there ever arose a need.

We've moved over to Calamares as the installer included in the ISO (similar to the Lubuntu and Ubuntu Studio installers on previous releases, if you've ever tried them, which for that matter, you definitely should!). We would like to thank Simon Quigley and Aaron Rainbolt for integrating Calamares with our existing Ubuntu Unity live session, and last-minute fixes for bugs reported prior to the final release.

![Ubuntu Lomiri 24.04 greeter](../../assets/images/lomiri-24.04-testing-greeter.png)

Now speaking of Lomiri, we're aware that for the past few years, a lot of you have been eagerly awaiting Lomiri desktop images. And well, for those of you, we have some thrilling news: the first 24.04 Lomiri testing ISO is now publicly available!

![Ubuntu Lomiri 24.04 app launcher](../../assets/images/lomiri-24.04-testing-launcher.png)

Over the past few years, the UBports team (special shout-out, in alphabetical order, to Alfred Neumayer, Marius Gripsgard and Mike Gabriel!) have been hard at work, improving desktop compatibility and getting every single one of the Lomiri packages into the Debian repositories, and subsequently, they made their way into the Ubuntu repositories too. Following which, we decided it would be a good idea to build an installable, daily-driveable ISO, for your ~~viewing~~ testing pleasure.

![Ubuntu Lomiri 24.04 window switcher](../../assets/images/lomiri-24.04-testing-windowswitcher.png)

The Ubuntu Lomiri ISO I have built can be found here: https://ruds.io/cloud/s/eNiJxnc7qk2tpKN

If the launcher/dock does not show up upon booting and you only see the Lomiri background, just press the Super/Windows key once for it (as well as dash to show up). The dock should automatically show up on subsequent logins.

![Firefox on Ubuntu Lomiri 24.04](../../assets/images/lomiri-24.04-testing-firefox.png)

Unlike the half-broken testing Lomiri ISOs I have built in the past, this one's quite stable and can be installed on real hardware, while also including the Ubuntu Touch LightDM greeter and for that matter, several apps you'd only expect to find on Ubuntu Touch. Do bear in mind, however, that you may encounter occasional crashes here and there.

### Upgrades from 23.10

If you're on a laptop, make sure it is sufficiently charged or plugged in. A stable internet connection is also preferred.

You must run the following commands:

```bash
sudo apt install ubuntu-unity-desktop ubuntu-release-upgrader-core
sudo do-release-upgrade -d
```

### Ubuntu Unity merch

Thanks to Maik Adamietz of the Ubuntu Unity team, we are glad to announce a collab between HelloTux and Ubuntu Unity.

From [HelloTux's website](https://www.hellotux.com/aboutus.php):

> We are not just another t-shirt shop with some Linux shirts - we are a shop with only Linux and free software shirts. It does really matter.

HelloTux is a family business and a global provider of shirts and merch for FOSS projects. We encourage you to check out their awesome merch, and do take a look at their Ubuntu Unity merch too!

https://www.hellotux.com/ubuntu-unity

### Additional information

You can also install Ubuntu Unity on an existing Ubuntu install by removing `gnome-shell` and other GNOME apps, and then installing the `ubuntu-unity-desktop` package
(all the Ubuntu Unity packages are in `universe` in the official Ubuntu archive).

You may encounter missing close-maximize-minimize buttons in GNOME apps (since they use custom header bars/client-side-decorations). Removing the `libgtk3-nocsd0` package should help with that.

**Special thanks to Dmitry Shachnev (mitya57), our MOTU/uploader, who has been uploading all our packages to the universe pocket of the official Ubuntu Unity repositories, including many last-minute emergency uploads. This release would not have been possible without his help and support.**

**I would also like to thank Maik and Tobiyo, fellow members of the Ubuntu Unity team, who have actively and patiently been answering all your support-related queries, and moderating our Telegram, Matrix, IRC and Discord groups/servers.**

For support or queries, please join us on Telegram at [t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss), or on IRC (`#ubuntu-unity` on libera.chat).

