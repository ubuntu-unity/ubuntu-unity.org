---
pubDatetime: 2023-04-20T00:00:00
title: "Ubuntu Unity 23.04 released"
description: "Ubuntu Unity 23.04 has now been released."
image: "images/blog/ubuntu-unity-becomes-an-official-flavor.png"
author: "Rudra Saraswat"
draft: false
featured: true

tags:
  - flavor
  - release
  - stable
---

Ubuntu Unity 23.04 "Lunar Lobster" has now been released! You can download it from https://ubuntuunity.org/download/.

### New changes

It is the first distribution to ship Unity 7.7 out-of-the-box, and brings many improvements to the table, including:

* A brand-new dash, similar to that displayed in the concepts designed for Ubuntu 16.04.
* A slightly bigger panel, that is translucent by default and looks much better in light mode.
* indicator-notification is installed by default, and allows you to see any notifications that may have popped up while you were away.
* The official widgets implementation, UWidgets, is now supported. We're currently working on a store for downloading and managing them, and it will be made available within the next couple of weeks.

And some changes:

* The Settings app's shell UI has been drastically improved.
* The launcher BFB is now half-transparent, and fits in with the rest of the default Yaru icons, similar to Ubuntu Unity's custom BFB for Unity 7.5 in 21.04.

### Ubuntu Unity merch

Thanks to Maik Adamietz of the Ubuntu Unity team, we are glad to announce a collab between HelloTux and Ubuntu Unity.

From [HelloTux's website](https://www.hellotux.com/aboutus.php):

> We are not just another t-shirt shop with some Linux shirts - we are a shop with only Linux and free software shirts. It does really matter.

HelloTux is a family business and a global provider of shirts and merch for FOSS projects. We encourage you to check out their awesome merch, and do take a look at their Ubuntu Unity merch too!

https://www.hellotux.com/ubuntu-unity

### Additional information

You can also install Ubuntu Unity on an existing Ubuntu install by removing `gnome-shell` and other GNOME apps, and then installing the `ubuntu-unity-desktop` package
(all the Ubuntu Unity packages are in `universe` in the official Ubuntu archive).

**Special thanks to Dmitry Shachnev (mitya57), our MOTU/uploader, who has been uploading all our packages to the universe pocket of the official Ubuntu Unity repositories, including many last-minute emergency uploads. This release would not have been possible without his help and support.**

**I would also like to thank Maik and Tobiyo, fellow members of the Ubuntu Unity team, who have actively and patiently been answering all your support-related queries, and moderating our Telegram, Matrix, IRC and Discord groups/servers.**

For support or queries, please join us on Telegram at [t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss), or on IRC (`#ubuntu-unity` on libera.chat).
